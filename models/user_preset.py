from typing import Dict, Union

from django.db import models
from django_mysql.models import ListTextField

import json

from metacanon_core.models.fields.json_field import JsonField
from metacanon_core.utils import normalize_list_of_ints

ORDERS = [
    ('title', 'Title'),
    ('score', 'Score'),
    ('year', 'Year'),
    ('author', 'Author')
]

GENRES = [
    ('novel', 'Novel'),
    ('collection', 'Collection of Short Stories'),
    ('novella', 'Novella'),
    ('other', 'Other')
]


class WeightsField(JsonField):
    """ Weights stored as a JSON object mapping data source names to floating point weights. """

    def to_python(self, value: Union[None, str, Dict]):
        return WeightsField._normalize(self._to_python(value))

    def get_prep_value(self, value):
        """ Convert python dictionary to JSON string before writing to db. """
        if value is None:
            return value
        else:
            weights = WeightsField._normalize(value)
            return json.dumps(weights)

    @staticmethod
    def _normalize(value: Dict):
        """ Convert any string representations of weights to floats. """
        return {k: WeightsField._normalize_float(v) for k, v in value.items()}

    @staticmethod
    def _normalize_float(value):
        if value is None:
            return 0.0
        else:
            return float(value)


class ListIntField(ListTextField):
    def from_db_value(self, value, expression, connection):
        value_list = super().from_db_value(value, expression, connection)
        return normalize_list_of_ints(value_list)


class UserPreset(models.Model):
    preset_id = models.AutoField(primary_key=True, unique=True)
    user_id = models.IntegerField(db_index=True)
    preset_name = models.CharField(max_length=50)
    total_books = models.IntegerField(choices=[(100, '100'), (500, '500'), (1000, '1000')])
    books_per_page = models.IntegerField(choices=[(25, '25'), (50, '50'), (100, '100')])
    ordered_by = models.TextField(choices=ORDERS)
    weights = WeightsField()
    one_work_per_author = models.BooleanField()
    women_only = models.BooleanField()
    genres = ListIntField(base_field=models.CharField(max_length=24))
    regions = ListIntField(base_field=models.CharField(max_length=24))
    tags = ListIntField(base_field=models.CharField(max_length=24))
    min_year = models.IntegerField(null=True)
    max_year = models.IntegerField(null=True)
    normalize_by_publication_year = models.BooleanField(default=False)
    faulkner = models.BooleanField()

    class Meta:
        db_table = "metacanon_core_user_preset"
