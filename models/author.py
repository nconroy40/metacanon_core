from django.db import models

from metacanon_core.models.fields.json_field import JsonField

GENDERS = [
    ('male', 'Male'),
    ('female', 'Female'),
    ('nonbinary', 'Non-binary'),
    ('other', 'Other'),
    ('none', 'None'),
    ('unknown', 'Unknown')
]


class Author(models.Model):
    class Meta:
        unique_together = ["first_name", "last_name", "date_of_birth"]

    author_id = models.AutoField(primary_key=True, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    alternate_names = JsonField(null=True)
    author_gender = models.TextField(choices=GENDERS, null=True)
    date_of_birth = models.DateField(null=True)
    date_of_death = models.DateField(null=True)

    # allows description of gender if author_gender is too restrictive
    gender_description = models.TextField(null=True, blank=True)
    name_corpus_frequency = models.IntegerField(null=True, blank=True)
