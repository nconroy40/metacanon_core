from django.db import connection
from pandas import Series
import numpy as np


def corpus_frequency_multiplier(tcf: Series, ncf: Series) -> Series:
    """
    :param tcf: title corpus frequency
    :param ncf: author name corpus frequency
    :return: multiplier for corpus frequency correction
    """
    def log(s: Series, d: int):
        """
        :return: NaN if log is less than 1
        """
        return np.log(s.fillna(1).apply(lambda x: 1 if x == 0 else x) / d).apply(lambda x: None if x <= 0 else x)

    return 1 / (log(tcf, 50) * log(ncf, 50)).fillna(1)


def hathi_corpus_frequency_multiplier(tcf: Series, ncf: Series) -> Series:
    """
    :param tcf: title corpus frequency
    :param ncf: author name corpus frequency
    :return: multiplier for corpus frequency correction
    """
    def log(s: Series, d: int):
        """
        :return: NaN if log is less than 1
        """
        return np.log(s.fillna(1).apply(lambda x: 1 if x == 0 else x) / d).apply(lambda x: None if x <= 0 else x)

    return (1 / (log(tcf, 10) * log(ncf, 10)).fillna(1)).apply(lambda x: 1 if x > 1 else x)


# noinspection SqlNoDataSourceInspection
def update_ngram_frequencies():
    with connection.cursor() as cursor:
        print("Updating title corpus frequencies.")
        cursor.execute(
            "UPDATE metacanon_09_work a "
            "JOIN metacanon_09_ngramfrequency b "
            "SET a.title_corpus_frequency = b.frequency "
            "WHERE LOWER(a.title) = LOWER(b.ngram);"
        )
        print(f"{cursor.rowcount} rows updated.")

        print("Updating author name corpus frequencies.")
        cursor.execute(
            "UPDATE metacanon_09_author a "
            "JOIN metacanon_09_ngramfrequency b "
            "SET a.name_corpus_frequency = b.frequency "
            "WHERE LOWER(CONCAT(a.first_name, ' ', a.last_name)) = LOWER(b.ngram);"
        )
        print(f"{cursor.rowcount} rows updated.")
