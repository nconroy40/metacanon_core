from .author import Author
from .work import Work
from .user_preset import UserPreset
from .citation_count import GoogleScholarCitationCount, JSTORCitationCount, JSTORLanguageAndLiteratureCitationCount, \
    ALHCitationCount, AmericanLiteratureCitationCount, NewYorkTimesCitationCount
from .region import Region
from .dbpedia_metadata import DbpediaWorkMetadata, DbpediaAuthorMetadata
from .data_source import DataSource
from .genre import Genre
from .tag import Tag
from .query_queue import QueryQueue
from .ngram_frequency import NgramFrequency
from .work_catalogued_by_user import WorkCataloguedByUser
from .award_record import PulitzerPrizeRecord
