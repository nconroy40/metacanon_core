import json
from django.db import models
from typing import Union, Dict


class JsonField(models.TextField):
    """ Field storing a JSON object """

    def to_python(self, value: Union[None, str, Dict]):
        if value is None:
            return []
        return self._to_python(value)

    def get_prep_value(self, value):
        """ Convert python dictionary to JSON string before writing to db. """
        if value is None:
            return value
        else:
            return json.dumps(value)

    # noinspection PyUnusedLocal
    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    @staticmethod
    def _to_python(value: Union[None, str, Dict]):
        """ Convert JSON string value to python dictionary. """
        if value is None:
            return value
        elif isinstance(value, dict):
            return value
        elif isinstance(value, str):
            return json.loads(value)
        else:
            raise ValueError("value must be either None, a dictionary, or a string.")
