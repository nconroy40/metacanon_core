from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import validate_comma_separated_integer_list

from django.db import models
from django_mysql.models import ListCharField

from metacanon_core.models import Author
from metacanon_core.models.fields.json_field import JsonField
from metacanon_core.models.region import Region

FORMAT_CHOICES = [
    ('book', 'book'),
    ('article', 'article')
]

STATUS_CHOICES = [
    ('pending', 'pending'),  # The work was added by an automated process and is waiting confirmation by a moderator.
    ('confirmed', 'confirmed'),  # The existence of the work has been confirmed by a moderator.
    ('on_hold', 'on_hold'),  # The work exists but a moderator has flagged it as not yet ready to be added.
    ('rejected', 'rejected')  # The work doesn't exist or is otherwise ineligible. E.g. things tagged as WrittenWork
    # in dbpedia that are not actually written works.
]


class Work(models.Model):
    work_id = models.AutoField(primary_key=True, unique=True)
    title = models.CharField(max_length=1000)
    alternate_titles = JsonField(null=True)
    author_id = models.ForeignKey(Author, on_delete=models.SET_NULL, null=True)
    publication_year = models.IntegerField(null=True)
    genres = ListCharField(validators=[validate_comma_separated_integer_list],
                           base_field=models.CharField(max_length=10), max_length=1000, null=True)
    format = models.CharField(choices=FORMAT_CHOICES, max_length=100, null=True, blank=True)
    tags = ListCharField(validators=[validate_comma_separated_integer_list], base_field=models.CharField(max_length=10),
                         max_length=1000, null=True)
    region = models.ForeignKey(to=Region, on_delete=models.CASCADE, null=True, blank=True)
    language = models.CharField(null=True, max_length=100, blank=True)
    title_corpus_frequency = models.IntegerField(null=True, blank=True)
    last_updated = models.DateTimeField(null=True)
    confirmation_status = models.CharField(choices=STATUS_CHOICES, null=False, max_length=20)

    @staticmethod
    def update_or_insert(author_id,
                         title,
                         last_updated,
                         title_corpus_frequency,
                         genres,
                         tags,
                         work_format,
                         language,
                         publication_year,
                         region):
        try:
            work = Work.objects.get(author_id=author_id, title=title)
        except ObjectDoesNotExist:
            work = Work(title=title, author_id=author_id)
        work.last_updated = last_updated
        work.title_corpus_frequency = title_corpus_frequency
        work.genres = genres
        work.tags = tags
        work.format = work_format
        work.publication_year = publication_year
        work.language = language
        work.region = Region.objects.get(region_id=region)
        work.save()
        return work

    @staticmethod
    def exists(author_id, title):
        # TODO: handle MultipleObjectsReturned exception
        try:
            Work.objects.get(author_id=author_id, title=title)
            return True
        except ObjectDoesNotExist:
            return False

    @staticmethod
    def find_or_create(author_id, title):
        try:
            work = Work.objects.get(author_id=author_id, title=title)
        except ObjectDoesNotExist:
            work = Work(author_id=author_id, title=title)
        return work
