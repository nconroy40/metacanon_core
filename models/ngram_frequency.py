from django.db import models


class NgramFrequency(models.Model):
    ngram = models.CharField(max_length=200, unique=True)
    frequency = models.IntegerField()

    class Meta:
        db_table = "metacanon_core_ngram_frequency"
