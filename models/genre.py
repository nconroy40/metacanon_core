from django.db import models


class Genre(models.Model):
    genre_id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=100, unique=True)
    human_readable_name = models.TextField()
    access_level = models.IntegerField()

    def __str__(self):
        return self.human_readable_name
