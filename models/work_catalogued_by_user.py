from django.db import models


STATUSES = [
    ('unread', 'unread'),
    ('want_to_read', 'want_to_read'),
    ('partially_read', 'partially_read'),
    ('read', 'read'),
]


class WorkCataloguedByUser(models.Model):
    user_id = models.IntegerField()
    work_id = models.IntegerField()
    status = models.TextField(choices=STATUSES)

    class Meta:
        unique_together = ('user_id', 'work_id')
        db_table = "metacanon_core_work_catalogued_by_user"
