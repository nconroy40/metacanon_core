from pandas import Series
import numpy as np

from metacanon_core.data.corpus_frequency_correction import corpus_frequency_multiplier


def test_corpus_frequency_multiplier():
    title_corpus_frequency = Series([3872477, 28467, 26039, 117358, 15063, None, 0, None, 22, 46038])
    author_name_corpus_frequency = Series([1151, 358, 180, 22, 0, None, 0, 1, None, 109])
    multiplier = corpus_frequency_multiplier(title_corpus_frequency, author_name_corpus_frequency)
    expected = Series([
        0.028322802411051177,
        0.0800693857985163,
        0.12480248525256683,
        1.0,
        1.0,
        1.0,
        1.0,
        1.0,
        1.0,
        0.18800357837140963
    ])
    print(multiplier)
    print(expected)
    assert np.isclose(multiplier, expected).all()
