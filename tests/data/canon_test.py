from math import isclose

import pytest
from django.utils.timezone import now

from metacanon_core.data import presets
from metacanon_core.data.canon import Canon
from metacanon_core.models import Author, Work, Region, UserPreset
from metacanon_core.tests.test_utils import create_db_defaults


@pytest.mark.django_db
def test_canon():
    create_db_defaults()
    canon = Canon(presets.default(), False).get()
    work1 = canon[0][0]
    work2 = canon[0][1]
    work3 = canon[0][2]
    assert work1['title'] == 'work3'
    assert work2['title'] == 'work2'
    assert work3['title'] == 'work1'


@pytest.mark.django_db
def test_canon_with_user():
    create_db_defaults()
    canon = Canon(presets.default(), False).get(user_id=1)
    work1 = canon[0][0]
    work2 = canon[0][1]
    work3 = canon[0][2]
    assert work1['status'] == 'unread'
    assert work2['status'] == 'read'
    assert work3['status'] == 'want_to_read'


@pytest.mark.django_db
def test_one_per_author():
    create_db_defaults()
    preset = presets.default()
    preset.one_work_per_author = True
    canon = Canon(preset, False).get()
    assert len(canon[0]) == 2
    assert canon[0][0]['title'] == 'work3'
    assert canon[0][1]['title'] == 'work1'


@pytest.mark.django_db
def test_average_citations_per_year():
    create_db_defaults()
    work = Work.objects.get(work_id=1)
    work.publication_year = now().year + 10  # Future publication date shouldn't cause an exception.
    work.save()
    preset = presets.default_no_filters()
    preset.min_year = 0
    preset.max_year = 3000
    preset.normalize_by_publication_year = True
    canon = Canon(preset, False).get()
    assert len(canon[0]) == 3


@pytest.mark.django_db
def test_women_only():
    create_db_defaults()
    preset = presets.default()
    preset.women_only = True
    canon = Canon(preset, False).get()
    assert len(canon[0]) == 2
    assert canon[0][0]['title'] == 'work3'
    assert canon[0][1]['title'] == 'work2'


@pytest.mark.django_db
def test_tags():
    create_db_defaults()
    preset = presets.default()
    preset.genres = [1, 2]
    preset.tags = [1]
    preset.regions = []
    canon = Canon(preset, False).get()

    # Should be only one work with this tag.
    assert len(canon[0]) == 1
    assert canon[0][0]['tags'] == [1]


@pytest.mark.django_db
def test_get_num_works_by_genre():
    create_db_defaults()
    num_works_by_genre = Canon(presets.default(), False).get_num_works_by_genre()
    genre1 = num_works_by_genre[0]
    genre2 = num_works_by_genre[1]
    genre3 = num_works_by_genre[2]
    genre4 = num_works_by_genre[3]
    assert genre1['name'] == 'genre1'
    assert genre1['count'] == 3
    assert genre1['percentage'] == 100.0
    assert genre2['name'] == 'genre2'
    assert genre2['count'] == 1
    assert abs(genre2['percentage'] - 33.33333333) < 0.0001
    assert genre3['name'] == 'genre3'
    assert genre3['count'] == 0
    assert genre4['name'] == 'genre4'
    assert genre4['count'] == 0


@pytest.mark.django_db
def test_get_num_works_by_gender():
    create_db_defaults()
    num_works_by_gender = Canon(presets.default(), False).get_num_books_by_gender()
    assert len(num_works_by_gender) == 2
    assert num_works_by_gender[0]["author_gender"] == "female"
    assert num_works_by_gender[0]["count"] == 2
    assert isclose(num_works_by_gender[0]["percentage"], (2/3) * 100)
    assert num_works_by_gender[1]["author_gender"] == "male"
    assert num_works_by_gender[1]["count"] == 1
    assert isclose(num_works_by_gender[1]["percentage"], (1/3) * 100)


@pytest.mark.django_db
def test_get_num_authors_by_gender():
    create_db_defaults()
    num_authors_by_gender = Canon(presets.default(), False).get_num_authors_by_gender()
    assert len(num_authors_by_gender) == 2
    assert num_authors_by_gender[0]["author_gender"] == "female"
    assert num_authors_by_gender[0]["count"] == 1
    assert isclose(num_authors_by_gender[0]["percentage"], (1/2) * 100)
    assert num_authors_by_gender[1]["author_gender"] == "male"
    assert num_authors_by_gender[1]["count"] == 1
    assert isclose(num_authors_by_gender[1]["percentage"], (1/2) * 100)


@pytest.mark.django_db
def test_num_works_by_gender():
    create_db_defaults()
    num_works_by_gender = Canon(presets.default(), False).get_num_books_by_gender()
    assert len(num_works_by_gender) == 2
    assert num_works_by_gender[0]['count'] == 2
    assert num_works_by_gender[1]['count'] == 1


@pytest.mark.django_db
def test_num_authors_by_gender():
    create_db_defaults()
    num_authors_by_gender = Canon(presets.default(), False).get_num_authors_by_gender()
    assert len(num_authors_by_gender) == 2
    assert num_authors_by_gender[0]['count'] == 1
    assert num_authors_by_gender[1]['count'] == 1


@pytest.mark.django_db
def test_num_works_per_author():
    create_db_defaults()
    num_works_per_author = Canon(presets.default(), False).get_work_count_per_author()
    assert len(num_works_per_author) == 2
    assert num_works_per_author[0]['count'] == 2
    assert num_works_per_author[1]['count'] == 1


@pytest.mark.django_db
def test_total_score_per_author():
    create_db_defaults()
    total_score_per_author = Canon(presets.default(), False).get_total_score_per_author()
    assert len(total_score_per_author) == 2
    assert total_score_per_author[0]['total_score'] > total_score_per_author[1]['total_score']


@pytest.mark.django_db
def test_no_faulkner():
    create_db_defaults()
    faulkner = Author(first_name="William", last_name="Faulkner", author_gender="male")
    faulkner.save()
    Work(title="faulkner_book",
         author_id=faulkner,
         publication_year=1933,
         genres=[1],
         format="book",
         tags=[],
         region=Region.objects.get(pk=1),
         language="English").save()
    preset = presets.default()
    preset.faulkner = False
    canon = Canon(preset, False).get()
    assert len(canon) > 0
    for page in canon:
        for work in page:
            assert not (work['first_name'] == 'William' and work['last_name'] == 'Faulkner')


@pytest.mark.django_db
def test_get_works_by_author():
    create_db_defaults()
    preset = presets.default_no_filters()
    canon = Canon(preset, False).get_works_by_author(author_id=2, user_id=1)
    assert len(canon) > 0
    assert canon[0][0]['title'] == "work3"
    assert canon[0][0]['status'] == "unread"
    assert canon[0][1]['title'] == "work2"
    assert canon[0][1]['status'] == "read"


@pytest.mark.django_db
def test_get_num_works_read_by_user():
    create_db_defaults()
    preset = presets.default_no_filters()
    assert Canon(preset, False).get_num_works_read_by_user(user_id=1) == 1


@pytest.mark.django_db
def test_get_user_wish_list():
    create_db_defaults()
    preset = presets.full()
    wish_list = Canon(preset, False).get_user_wish_list(user_id=1)
    assert len(wish_list[0]) == 1
    assert wish_list[0][0]['title'] == "work1"
    assert wish_list[0][0]['nyt__score'] == 3.872983346207417


@pytest.mark.django_db
def test_works_filtered_out_if_missing_citations_for_included_source():
    create_db_defaults()
    weights = {"nyt": 1.0, "hathi_trust": 1.0}
    preset = UserPreset(
        preset_id=-1,
        user_id=-1,
        preset_name="default",
        total_books=500,
        books_per_page=100,
        ordered_by='score',
        weights=weights,
        one_work_per_author=False,
        women_only=False,
        genres=[1, 2, 3, 4],
        regions=[1],
        tags=[],
        min_year=0,
        max_year=3000,
        faulkner=True)
    canon = Canon(preset, False).get()

    # Work 3 is filtered out because it doesn't have an nyt citation count.
    assert set([w["work_id"] for w in canon[0]]) == {1, 2}
