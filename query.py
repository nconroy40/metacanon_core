import logging

from metacanon_core.models import Work, Author

from django.core.exceptions import ObjectDoesNotExist


logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger("django.db.backends").setLevel(logging.ERROR)
logger = logging.getLogger("build_query_queue")
logger.setLevel(logging.INFO)


class NoDbpediaMetadataError(Exception):
    pass


def get_query_args_for_type(query_type: str, work: Work = None, author: Author = None):
    # One and only one of these must be null.
    assert (work is None) != (author is None)

    if query_type == "wikipedia_backlink_citation_count":
        try:
            page_title = work.dbpedia_work_metadata.resource.split("/")[-1]
            return {"page_title": page_title}
        except ObjectDoesNotExist:
            raise NoDbpediaMetadataError
    elif work is None:
        return {"author_first_name": author.first_name, "author_last_name": author.last_name}
    else:
        try:
            args = {
                "author_first_name": work.author_id.first_name,
                "author_last_name": work.author_id.last_name,
                "titles": [work.title] + work.alternate_titles
            }
        except TypeError:
            logger.error(f"Title: {work.title}")
            logger.error(f"Alternate titles: {work.alternate_titles}")
            raise

        if query_type in ("hathi_citation_count", "hathi_title_frequency"):
            args["publication_year"] = work.publication_year
        return args
