import pytest
from django.utils import timezone

from metacanon_core.models import GoogleScholarCitationCount, Work


@pytest.mark.django_db
def test_citation_count():
    now = timezone.now()
    Work(work_id=1, publication_year=1999).save()
    GoogleScholarCitationCount.update_or_insert(entry_id=1, count=23, time=now)
    citation_count = GoogleScholarCitationCount.objects.get(work_id=1)
    assert citation_count.count == 23
    assert citation_count.last_updated == now
