from datetime import datetime

from abc import abstractmethod
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from pytz import utc

from metacanon_core.models import Work, Author


class CitationCount(models.Model):
    count = models.IntegerField(null=True)
    last_queried = models.DateTimeField(default=datetime(1970, 1, 1, 0, tzinfo=utc), null=True)
    last_updated = models.DateTimeField(default=datetime(1970, 1, 1, 0, tzinfo=utc), null=True)

    class Meta:
        abstract = True

    @classmethod
    def update_or_insert(cls, entry_id: int, count: int, time: datetime = datetime(1970, 1, 1, 0, tzinfo=utc)):
        citation_count_entry = cls._update_or_create(entry_id, count, time)
        citation_count_entry.save()

    @classmethod
    def _update_or_create(cls, entry_id: int, count: int, time):
        try:
            citation_count_entry = cls.get_by_id(entry_id)
            count_has_changed = citation_count_entry.count != count
        except ObjectDoesNotExist:
            if hasattr(cls, "work_id"):
                citation_count_entry = cls(work_id=Work.objects.get(work_id=entry_id))
            elif hasattr(cls, "author_id"):
                citation_count_entry = cls(author_id=Author.objects.get(author_id=entry_id))
            else:
                raise RuntimeError(f"Failed to create new citation count entry for {entry_id} for {cls}.")
            count_has_changed = True
        citation_count_entry.count = count
        citation_count_entry.last_queried = time
        if count_has_changed:
            citation_count_entry.last_updated = time
        return citation_count_entry

    @classmethod
    def exists(cls, entry_id: int):
        try:
            cls.get_by_id(entry_id=entry_id)
            return True
        except ObjectDoesNotExist:
            return False

    @classmethod
    def get_by_id(cls, entry_id: int):
        if hasattr(cls, "work_id"):
            return cls.objects.get(work_id=entry_id)
        elif hasattr(cls, "author_id"):
            return cls.objects.get(author_id=entry_id)
        else:
            raise RuntimeError(f"Failure when attempting to retrieve citation count for {entry_id} for class {cls}.")

    @staticmethod
    @abstractmethod
    def name():
        pass


class GoogleScholarCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='google_scholar')
    cluster_id = models.CharField(null=True, max_length=100)

    @classmethod
    def update_or_insert(
            cls,
            entry_id: int,
            count: int,
            time: datetime = datetime(1970, 1, 1, 0, tzinfo=utc),
            cluster_id: int = None):
        citation_count_entry = cls._update_or_create(entry_id, count, time)
        if cluster_id is not None:
            citation_count_entry.cluster_id = cluster_id
        citation_count_entry.save()

    @staticmethod
    def name():
        return 'Google Scholar'

    class Meta:
        db_table = "metacanon_core_citation_count_google_scholar"


class JSTORCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='jstor')

    @staticmethod
    def name():
        return 'JSTOR'

    class Meta:
        db_table = "metacanon_core_citation_count_jstor"


class ALHCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='alh')

    @staticmethod
    def name():
        return 'ALH'

    class Meta:
        db_table = "metacanon_core_citation_count_alh"


class AmericanLiteratureCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='american_literature')

    @staticmethod
    def name():
        return 'American Literature'

    class Meta:
        db_table = "metacanon_core_citation_count_american_literature"


class NewYorkTimesCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='nyt')
    nyt_corpus_frequency = models.IntegerField(default=0)

    @staticmethod
    def name():
        return 'New York Times'

    class Meta:
        db_table = "metacanon_core_citation_count_nyt"


class JSTORLanguageAndLiteratureCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True,
                                   related_name='jstor_language_and_literature')

    @staticmethod
    def name():
        return 'JSTOR Language and Literature'

    class Meta:
        db_table = "metacanon_core_citation_count_jstor_language_and_literature"


class HathiTrustCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True,
                                   related_name='hathi_trust')

    @staticmethod
    def name():
        return 'Hathi Trust'

    class Meta:
        db_table = "metacanon_core_citation_count_hathi_trust"


class HathiTrustTitleFrequency(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True,
                                   related_name='hathi_trust_title_corpus_frequency')

    @staticmethod
    def name():
        return 'Hathi Trust Title Frequency'

    class Meta:
        db_table = "metacanon_core_hathi_trust_title_frequency"


class HathiTrustAuthorFrequency(CitationCount):
    author_id = models.OneToOneField(
        Author, on_delete=models.CASCADE, primary_key=True, related_name='hathi_trust_author_corpus_frequency'
    )

    @staticmethod
    def name():
        return 'Hathi Trust Author Frequency'

    class Meta:
        db_table = "metacanon_core_hathi_author_title_frequency"


class WikipediaBacklinkCitationCount(CitationCount):
    work_id = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True,
                                   related_name='wikipedia_backlink')

    @staticmethod
    def name():
        return 'Wikipedia Backlink Citation Count'

    class Meta:
        db_table = "metacanon_core_citation_count_wikipedia_backlink"

