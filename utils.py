import os


def normalize_list_of_ints(x):
    if x is None:
        return []
    else:
        return [int(value) for value in x]


def subpath() -> str:
    return os.environ.get('METACANON_SITE_SUBPATH')
