from django.db import models

from metacanon_core.models import Author, Work


class DbpediaAuthorMetadata(models.Model):
    author = models.OneToOneField(
        Author,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='dbpedia_author_metadata'
    )
    resource = models.CharField(max_length=200, unique=True)
    gender = models.CharField(max_length=1000, null=True)
    birth_date = models.CharField(max_length=1000, null=True)
    death_date = models.CharField(max_length=1000, null=True)

    class Meta:
        db_table = "metacanon_core_dbpedia_author_metadata"


class DbpediaWorkMetadata(models.Model):
    work = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='dbpedia_work_metadata')
    resource = models.CharField(max_length=200, unique=True)
    publication_date = models.CharField(max_length=1000, null=True)
    publication_year = models.IntegerField(null=True)
    wiki_abstract = models.TextField(null=True)

    class Meta:
        db_table = "metacanon_core_dbpedia_work_metadata"
