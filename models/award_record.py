from datetime import datetime

from django.db import models

from metacanon_core.models import Work

AWARD_STATUSES = [
    ('won', 'won'),
    ('nominated', 'nominated'),
    ('none', 'none')
]


class AwardRecord(models.Model):
    award_status = models.TextField(choices=AWARD_STATUSES, default='none')
    last_updated = models.DateTimeField(default=datetime(1970, 1, 1, 0), null=True)

    class Meta:
        abstract = True


class PulitzerPrizeRecord(AwardRecord):
    work = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='pulitzer')
    pass

    class Meta:
        db_table = "metacanon_core_pulitzer_prize_record"


class NationalBookAwardRecord(AwardRecord):
    work = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='nba')
    pass

    class Meta:
        db_table = "metacanon_core_national_book_award_record"
