from django.db import models


NGRAM_OPTIONS = [
    ('coca', 'coca'),
    ('hathi_trust', 'hathi_trust'),
    ('nyt', 'nyt')
]

SOURCE_TYPE_OPTIONS = [
    ('citation_count', 'citation_count'),
    ('award_status', 'award_status')
]


class DataSource(models.Model):
    """
    Metadata describing a source of data (usually a citation count) used in aggregated scores for works.
    """
    name = models.CharField(max_length=50)
    human_readable_name = models.CharField(max_length=100)
    deprecated = models.BooleanField(default=False)
    access_level = models.IntegerField()
    ngram_frequency_correction_strategy = models.CharField(choices=NGRAM_OPTIONS, null=True, max_length=50)
    source_type = models.CharField(choices=SOURCE_TYPE_OPTIONS, null=False, max_length=50)

    class Meta:
        db_table = "metacanon_core_data_source"
