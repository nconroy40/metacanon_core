import pytest

from metacanon_core.models import Work, Author


@pytest.mark.django_db
def test_alternate_titles():
    author = Author(
        first_name="Fake",
        last_name="Name",
    )
    author.save()

    work = Work(
        title="fake title",
        alternate_titles=['alternate_title_one', 'alternate_title_two'],
        author_id=author,
        publication_year=1985,
        confirmation_status="pending"
    )

    work.save()

    retrieved_work = Work.objects.get(title="fake title")

    assert retrieved_work.alternate_titles == ['alternate_title_one', 'alternate_title_two']
