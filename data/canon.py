from functools import reduce

from django.db.models.functions import Cast
from django.utils import timezone
from typing import Dict, List
from django.db.models import QuerySet, F, FloatField
from django_pandas.io import read_frame
from math import sqrt
from pandas import DataFrame

from metacanon_core.data.corpus_frequency_correction import corpus_frequency_multiplier, \
    hathi_corpus_frequency_multiplier
from metacanon_core.models import UserPreset, Work, Genre, WorkCataloguedByUser, DataSource
from metacanon_core.utils import normalize_list_of_ints


class Canon:
    def __init__(self, preset: UserPreset, is_superuser: bool):
        self.preset: UserPreset = preset
        self._is_superuser = is_superuser

    def get(self, user_id: int = None) -> List[Dict]:
        canon = self._get_df(user_id=user_id).to_dict('records')
        num_pages = len(canon) // self.preset.books_per_page
        if len(canon) % self.preset.books_per_page != 0:
            num_pages += 1
        pages = []
        for i in range(num_pages):
            first_work = i * self.preset.books_per_page
            if i == num_pages - 1:
                pages.append(canon[i * self.preset.books_per_page:])
            else:
                last_work = first_work + self.preset.books_per_page
                pages.append(canon[i * self.preset.books_per_page:last_work])
        return pages

    def get_works_by_author(self, author_id: int, user_id: int = None):
        works: DataFrame = self._get_included_works(self.preset, author_id)

        if not works.empty:
            if self.preset.ordered_by == 'title':
                works = works.sort_values(['title'])
            elif self.preset.ordered_by == 'year':
                works = works.sort_values(['publication_year'])
            elif self.preset.ordered_by == 'score':
                works = works.sort_values(['score'], ascending=False)
            else:
                raise ValueError(f"Cannot order author page by {self.preset.ordered_by}.")

            works = works.reset_index()
            works['rank'] = works.index + 1
            works = self._add_user_statuses(user_id, works)

        return [works.to_dict('records')]

    def get_works_missing_cluster_id(self, user_id: int = None):
        works: DataFrame = self._get_included_works_no_limit(self.preset)
        works = works[works['google_scholar_cluster_id'].isnull()]
        works = works[works['google_scholar_last_updated'].isnull()]

        if not works.empty:
            works = works[:self.preset.total_books]
            works = works.sort_values(['score'], ascending=False)
            works = works.reset_index()
            works['rank'] = works.index + 1
            works = self._add_user_statuses(user_id, works)

        return [works.to_dict('records')]

    def get_num_books_by_gender(self) -> QuerySet:
        works = self._get_included_works(self.preset)
        num_works_by_gender = works.groupby('author_gender').agg({'work_id': 'count'}).reset_index()
        num_works_by_gender = num_works_by_gender.rename(columns={'work_id': "count"})
        num_works_by_gender['percentage'] = num_works_by_gender['count'] * 100.0 / len(works)
        return num_works_by_gender.to_dict('records')

    def get_num_authors_by_gender(self):
        works = self._get_included_works(self.preset)
        authors = works[['author_id', 'author_gender']].drop_duplicates()
        num_authors_per_gender = authors.groupby('author_gender').agg({'author_id': 'count'}).reset_index()
        num_authors_per_gender = num_authors_per_gender.rename(columns={'author_id': "count"})
        num_authors_per_gender['percentage'] = num_authors_per_gender['count'] * 100.0 / len(authors)
        return num_authors_per_gender.to_dict('records')

    def get_num_works_by_genre(self):
        works = self._get_included_works(self.preset)
        genres = read_frame(Genre.objects.filter(access_level=1))
        for i in range(len(genres)):
            genre_id = genres.at[i, 'genre_id']
            mask = works['genres'].apply(lambda x: any(item for item in x if item == genre_id))
            genres.at[i, 'count'] = len(works[mask])
        num_works = len(works)
        genres['percentage'] = genres['count'] * (100.0 / num_works)
        return genres.to_dict('records')

    def get_work_count_per_author(self):
        works = self._get_included_works(self.preset)
        num_works_per_author = works.groupby(['author_id', 'first_name', 'last_name']).agg(
            {'work_id': 'count'}).reset_index()
        num_works_per_author = num_works_per_author.rename(columns={'work_id': 'count'})
        num_works_per_author = num_works_per_author.sort_values(['count'], ascending=False)[:25]
        return num_works_per_author.to_dict('records')

    def get_total_score_per_author(self):
        works = self._get_included_works(self.preset)
        total_score_per_author = works.groupby(['author_id', 'first_name', 'last_name']).agg(
            {'score': 'sum'}).reset_index()
        total_score_per_author = total_score_per_author.rename(columns={'score': 'total_score'})
        total_score_per_author = total_score_per_author.sort_values(['total_score'], ascending=False)[:25]
        return total_score_per_author.to_dict('records')

    def get_num_works_read_by_user(self, user_id) -> int:
        canon = self._get_df(user_id=user_id)
        works_read_by_user = canon[canon['status'] == 'read'].to_dict('records')
        return len(works_read_by_user)

    def get_user_wish_list(self, user_id) -> List[Dict]:
        canon = self._get_df(user_id=user_id)
        return [canon[canon['status'] == 'want_to_read'].to_dict('records')]

    def _get_df(self, user_id: int = None) -> DataFrame:
        works: DataFrame = self._get_included_works(self.preset)

        if self.preset.ordered_by == 'author':
            works = works.sort_values(['last_name', 'first_name'])
        elif self.preset.ordered_by == 'title':
            works = works.sort_values(['title'])
        elif self.preset.ordered_by == 'year':
            works = works.sort_values(['publication_year'])
        elif self.preset.ordered_by == 'score':
            works = works.sort_values(['score'], ascending=False)
        else:
            raise ValueError(f"Cannot order by {self.preset.ordered_by}. Order type does not exist.")

        works = self._add_user_statuses(user_id, works)

        works = works.reset_index()
        works['rank'] = works.index + 1

        return works

    @staticmethod
    def _add_user_statuses(user_id, works):
        if user_id is not None:
            work_ids_for_user = read_frame(
                WorkCataloguedByUser.objects.filter(user_id=user_id).values("work_id", "status"))
            works = works.join(work_ids_for_user.set_index('work_id'), on='work_id')
            works['status'] = works['status'].fillna('unread')
        else:
            works['status'] = 'unread'
        return works

    def _get_included_works(self, preset, author_id: int = None) -> DataFrame:
        works_df = Canon._get_included_works_no_limit(preset, author_id)
        if not self._is_superuser:
            works_df = works_df[works_df.confirmation_status == 'confirmed']
        works_df = works_df[:preset.total_books]
        return works_df

    @staticmethod
    def _get_included_works_no_limit(preset, author_id: int = None):
        works: QuerySet = Work.objects
        works = works.select_related("author_id", "author_id__hathi_trust_author_corpus_frequency").\
            filter(
                publication_year__gte=preset.min_year,
                publication_year__lte=preset.max_year,
            ).exclude(confirmation_status='rejected')

        if preset.women_only:
            works = works.filter(author_id__author_gender__in=('Female', 'female', 'transgender female'))
        if author_id is not None:
            works = works.filter(author_id__author_id=author_id)

        if preset.regions is not None and len(preset.regions) > 0:
            works = works.filter(region__in=preset.regions)
        # add citation counts
        non_zero_weights = {k: v for k, v in preset.weights.items() if v != 0.0}
        for source in non_zero_weights:
            if source in ('nba', 'pulitzer'):
                works = works.annotate(**{f"{source}__award_status": F(f'{source}__award_status')})
            else:
                if preset.normalize_by_publication_year:
                    # Use average citations per year rather than total citations.
                    works = works.annotate(**{
                        f"{source}__count":
                            Cast(f'{source}__count', output_field=FloatField()) /
                            (timezone.now().year - Cast("publication_year", output_field=FloatField())
                             + 1.0)
                    })
                else:
                    works = works.annotate(**{f"{source}__count": F(f'{source}__count')})

            if source == 'nyt':
                works = works.annotate(**{"nyt_corpus_frequency": F(f'nyt__nyt_corpus_frequency')})

            if source == 'google_scholar':
                works = works.annotate(**{"google_scholar_cluster_id": F(f"google_scholar__cluster_id")})
                works = works.annotate(**{"google_scholar_last_updated": F(f"google_scholar__last_updated")})

        works = works.annotate(
            **{
                "hathi_author_citation_frequency": F('author_id__hathi_trust_author_corpus_frequency__count'),
                "author_gender": F('author_id__author_gender'),
                "first_name": F('author_id__first_name'),
                "last_name": F("author_id__last_name"),
                "name_corpus_frequency": F("author_id__name_corpus_frequency"),
                "author_identifier": F("author_id__author_id"),
            }
        )

        works_df: DataFrame = read_frame(works)
        works_df = Canon._filter_out_null_citation_counts(works_df, preset.weights)
        works_df = Canon._add_scores(works_df, preset.weights)
        if preset.genres is not None and len(preset.genres) > 0:
            # cast genres to ints
            works_df['genres'] = works_df['genres'].transform(normalize_list_of_ints)

            # filter down to only works that have at least one genre in the preset genres list
            mask = works_df['genres'].apply(lambda x: any(item for item in preset.genres if item in x))
            works_df = works_df[mask]
        if preset.tags is not None and len(preset.tags) > 0:
            # cast tags to ints
            works_df['tags'] = works_df['tags'].transform(normalize_list_of_ints)

            # filter down to only works that have at least one genre in the preset genres list
            mask = works_df['tags'].apply(lambda x: any(item for item in preset.tags if item in x))
            works_df = works_df[mask]
        if not preset.faulkner:
            works_df = works_df[(works_df['first_name'] != 'William') | (works_df['last_name'] != 'Faulkner')]
        if not works_df.empty:
            works_df = works_df.sort_values(by=['score'], ascending=False)
        if preset.one_work_per_author:
            works_df = works_df.drop_duplicates(['author_id'])
        return works_df

    @staticmethod
    def _filter_out_null_citation_counts(works: DataFrame, weights: Dict[str, float]):
        sources = [k for k, v in weights.items() if v != 0.0 and k not in ('nba', 'pulitzer')]
        condition = reduce(lambda a, b: a & b, [works[f"{source}__count"].notnull() for source in sources])
        return works[condition]

    @staticmethod
    def _add_scores(works: DataFrame, weights: Dict[str, float]):
        non_zero_weights = {k: v for k, v in weights.items() if v != 0.0}
        works['score'] = 0.0

        def award_status_to_score(x: str):
            if x == 'won':
                return 1.0
            elif x == 'nominated':
                return 0.5
            else:
                return 0.0

        for source_name in non_zero_weights:
            source = DataSource.objects.get(name=source_name)
            score_column = f"{source_name}__score"
            if source.source_type == "award_status":
                award_status_column = f"{source_name}__award_status"
                works[score_column] = works[award_status_column].apply(award_status_to_score) * weights[source_name]
            elif source.source_type == "citation_count":
                count_column = f"{source_name}__count"
                if source.ngram_frequency_correction_strategy == "coca":
                    corpus_frequency_factor = corpus_frequency_multiplier(
                        works["title_corpus_frequency"], works["name_corpus_frequency"]
                    )
                    works["corpus_frequency_correction_applied"] = corpus_frequency_factor != 1
                    works[count_column] = works[count_column] * corpus_frequency_factor
                elif source.ngram_frequency_correction_strategy == "nyt":
                    nyt_corpus_frequency_factor = corpus_frequency_multiplier(
                        works['nyt_corpus_frequency'], works["name_corpus_frequency"]
                    )
                    works["nyt_corpus_frequency_correction_applied"] = nyt_corpus_frequency_factor != 1
                    works[count_column] = works[count_column] * nyt_corpus_frequency_factor
                elif source.ngram_frequency_correction_strategy == "hathi_trust":
                    try:
                        corpus_frequency_factor = hathi_corpus_frequency_multiplier(
                            works["hathi_title_citation_frequency"], works["hathi_author_citation_frequency"]
                        )
                        works["corpus_frequency_correction_applied"] = corpus_frequency_factor != 1
                        works[count_column] = works[count_column] * corpus_frequency_factor
                    except KeyError:
                        raise RuntimeError(works.columns)
                elif source.ngram_frequency_correction_strategy is None:
                    # ngram frequency correction will not be applied in this case
                    pass
                else:
                    raise RuntimeError(
                        f"Unknown ngram_frequency_correction_strategy: {source.ngram_frequency_correction_strategy}."
                    )

                def score_apply(x: float):
                    if x < 0:
                        # This number should only be negative if the work is published in the future,
                        # which shouldn't be the case.
                        return 0
                    else:
                        return sqrt(x) * weights[source_name]
                works[score_column] = works[count_column].apply(score_apply)
            else:
                raise RuntimeError(f"Unknown source_type: {source.source_type}.")

            works['score'] += works[score_column]

        return works
