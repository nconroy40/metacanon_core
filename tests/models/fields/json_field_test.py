from json import JSONDecodeError

import pytest

from metacanon_core.models.fields.json_field import JsonField


def test_to_python():
    field = JsonField()

    assert field.to_python(None) == []
    assert field.to_python('{"hey": 1}') == {"hey": 1}
    assert field.to_python('["one", "two"]') == ["one", "two"]

    # Should fail on invalid JSON
    with pytest.raises(JSONDecodeError):
        field.to_python("['one', 'two']")


def test_get_prep_value():
    field = JsonField()

    assert field.get_prep_value([]) == "[]"
    assert field.get_prep_value(None) is None
    assert field.get_prep_value(['one', 'two']) == '["one", "two"]'
    assert field.get_prep_value({}) == "{}"
    assert field.get_prep_value({"hey": 1}) == '{"hey": 1}'
