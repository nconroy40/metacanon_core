from django.db import models

from metacanon_core.models import Work


class LocMetadata(models.Model):
    work = models.OneToOneField(Work, on_delete=models.CASCADE, primary_key=True, related_name='loc_metadata')
    publication_year = models.IntegerField(null=True)
    loc_title = models.CharField(max_length=1000, null=True)
    loc_date = models.CharField(max_length=1000, null=True)
    loc_author = models.CharField(max_length=1000, null=True)
    loc_format = models.CharField(max_length=1000, null=True)

    class Meta:
        db_table = "metacanon_core_loc_metadata"
