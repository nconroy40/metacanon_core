from django.db import models

from metacanon_core.models.fields.json_field import JsonField


class QueryQueue(models.Model):
    work_id = models.IntegerField(null=True)
    author_id = models.IntegerField(null=True)
    query_args = JsonField(null=False)
    query_type = models.CharField(null=False, max_length=100)
    force_query = models.BooleanField(null=True)
    status = models.CharField(null=False, max_length=100, default="ready")
    error = models.TextField(null=True)

    class Meta:
        db_table = "metacanon_core_query_queue"

    def get_entry_id(self):
        if self.work_id is None:
            return self.author_id
        else:
            return self.work_id
