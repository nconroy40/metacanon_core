import pytest
from typing import Dict

from metacanon_core.citations.query_analytics import query_queue_summary
from metacanon_core.models import QueryQueue


@pytest.mark.django_db
def test_queue_data():
    _make_queue_entry("some_query_type", "ready").save()
    _make_queue_entry("some_query_type", "ready").save()
    _make_queue_entry("another_query_type", "ready").save()
    _make_queue_entry("another_query_type", "ready").save()
    _make_queue_entry("another_query_type", "ready").save()
    _make_queue_entry("another_query_type", "ready").save()
    _make_queue_entry("another_query_type", "ready").save()
    _make_queue_entry("another_query_type", "failed").save()
    _make_queue_entry("another_query_type", "failed").save()
    _make_queue_entry("third_query_type", "ready").save()
    _make_queue_entry("third_query_type", "failed").save()

    summary: Dict = query_queue_summary()
    assert summary == {
        "some_query_type": {"ready": 2},
        "another_query_type": {"ready": 5, "failed": 2},
        "third_query_type": {"ready": 1, "failed": 1}
    }


def _make_queue_entry(query_type: str, status: str):
    return QueryQueue(
        work_id=1,
        author_id=None,
        query_args={"arg": "value", "another_arg": "value"},
        query_type=query_type,
        force_query=False,
        status=status,
        error=None,
    )
