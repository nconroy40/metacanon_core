import datetime
from typing import Dict

from metacanon_core.models import UserPreset


def default() -> UserPreset:
    weights = _default_weights()
    return _default(weights, min_year=1900, max_year=1999)


def nineteenth():
    weights = _default_weights()
    return _default(weights, min_year=1800, max_year=1899)


def eighteen_hundred_to_present():
    weights = _default_weights()
    return _default(weights, min_year=1800, max_year=datetime.datetime.now().year)


def nyt_archive():
    weights = {'nyt': 1.0}
    return _default(weights, min_year=1700, max_year=datetime.datetime.now().year)


def default_no_filters():
    default_preset = _default(_default_weights(), min_year=-2000000, max_year=2000000)
    default_preset.genres = []
    default_preset.tags = []
    default_preset.regions = []
    default_preset.total_books = 2000000
    return default_preset


def full():
    weights = {
        'alh': 0.0,
        'nyt': 1.0,
        'american_literature': 0.0,
        'jstor': 0.0,
        'jstor_language_and_literature': 0.0,
        'google_scholar': 0.0,
        'hathi_trust': 1.0,
        'nba': 0.0,
        'pulitzer': 0.0
    }
    default_preset = _default(weights, min_year=-2000000, max_year=2000000)
    default_preset.genres = []
    default_preset.tags = []
    default_preset.regions = []
    default_preset.total_books = 2000000
    return default_preset


def _default_weights():
    return {'alh': 1.0,
            'nyt': 0.0,
            'american_literature': 1.0,
            'jstor': 0.0,
            'jstor_language_and_literature': 1.0,
            'google_scholar': 0.5,
            'nba': 0.0,
            'pulitzer': 0.0,
            'hathi_trust': 0.0}


def _default(weights: Dict[str, float], min_year: int, max_year: int):
    return UserPreset(preset_id=-1,
                      user_id=-1,
                      preset_name="default",
                      total_books=500,
                      books_per_page=100,
                      ordered_by='score',
                      weights=weights,
                      one_work_per_author=False,
                      women_only=False,
                      genres=[1, 2, 3, 4],
                      regions=[1],
                      tags=[],
                      min_year=min_year,
                      max_year=max_year,
                      faulkner=True)
