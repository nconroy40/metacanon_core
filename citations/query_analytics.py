from django_pandas.io import read_frame
from metacanon_core.models import QueryQueue


def query_queue_summary():
    df = read_frame(QueryQueue.objects.all())
    summarized = df\
        .groupby(by=['query_type', 'status'])\
        .agg({"id": "count"})\
        .reset_index()\
        .rename(columns={"id": "count"})\
        .groupby(by=["query_type"])

    return {
        k: dict({row['status']: row['count'] for index, row in v.iterrows()})
        for k, v in dict(tuple(summarized)).items()
    }
