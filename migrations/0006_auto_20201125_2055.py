# Generated by Django 2.2 on 2020-11-25 20:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metacanon_core', '0005_genre_ngramfrequency_queryqueue_tag_userpreset_workcataloguedbyuser'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='ngramfrequency',
            table='metacanon_core_ngram_frequency',
        ),
        migrations.AlterModelTable(
            name='queryqueue',
            table='metacanon_core_query_queue',
        ),
        migrations.AlterModelTable(
            name='userpreset',
            table='metacanon_core_user_preset',
        ),
        migrations.AlterModelTable(
            name='workcataloguedbyuser',
            table='metacanon_core_work_catalogued_by_user',
        ),
    ]
