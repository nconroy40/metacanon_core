from django.test import RequestFactory

from metacanon_core.models import Genre, Region, Author, Work, JSTORLanguageAndLiteratureCitationCount, \
    GoogleScholarCitationCount, ALHCitationCount, AmericanLiteratureCitationCount, Tag, WorkCataloguedByUser, \
    NewYorkTimesCitationCount, DataSource
from metacanon_core.models.citation_count import HathiTrustCitationCount, JSTORCitationCount


def _generate_tag(number: int) -> Tag:
    tag = Tag(name=f"tag{number}", human_readable_name=f"Tag {number}", access_level=2)
    tag.save()
    return tag


def create_db_defaults():
    Genre(name='genre1', human_readable_name="Genre 1", access_level=1).save()
    Genre(name='genre2', human_readable_name="Genre 2", access_level=1).save()
    Genre(name='genre3', human_readable_name="Genre 3", access_level=1).save()
    Genre(name='genre4', human_readable_name="Genre 4", access_level=1).save()
    Genre(name='genre5', human_readable_name="Genre 5", access_level=2).save()

    for i in range(0, 11):
        _generate_tag(i)

    region1 = Region(name='region1', human_readable_name="Region 1", access_level=1)
    region1.save()
    author_one = Author(first_name='Joe', last_name='Blow', author_gender='male')
    author_one.save()
    author_two = Author(first_name='Sarah', last_name='Dara', author_gender='female')
    author_two.save()
    work1 = Work(title='work1',
                 author_id=author_one,
                 publication_year=1934,
                 genres=[1, 2],
                 format="book",
                 tags=[1],
                 region=region1,
                 language="English",
                 confirmation_status='confirmed')
    work1.save()
    work2 = Work(title='work2',
                 author_id=author_two,
                 publication_year=1964,
                 genres=[1],
                 format="book",
                 tags=[],
                 region=region1,
                 language="English",
                 confirmation_status='confirmed')
    work2.save()
    work3 = Work(title='work3',
                 author_id=author_two,
                 publication_year=1985,
                 genres=[1],
                 format="book",
                 tags=[10],
                 region=region1,
                 language="English",
                 confirmation_status='confirmed')
    work3.save()
    JSTORCitationCount(work_id=work1, count=12).save()
    JSTORCitationCount(work_id=work2, count=22).save()
    JSTORCitationCount(work_id=work3, count=25).save()
    JSTORLanguageAndLiteratureCitationCount(work_id=work1, count=10).save()
    JSTORLanguageAndLiteratureCitationCount(work_id=work2, count=20).save()
    JSTORLanguageAndLiteratureCitationCount(work_id=work3, count=23).save()
    GoogleScholarCitationCount(work_id=work1, count=10).save()
    GoogleScholarCitationCount(work_id=work2, count=20).save()
    GoogleScholarCitationCount(work_id=work3, count=22).save()
    ALHCitationCount(work_id=work1, count=10).save()
    ALHCitationCount(work_id=work2, count=20).save()
    ALHCitationCount(work_id=work3, count=22).save()
    AmericanLiteratureCitationCount(work_id=work1, count=10).save()
    AmericanLiteratureCitationCount(work_id=work2, count=20).save()
    AmericanLiteratureCitationCount(work_id=work3, count=22).save()
    NewYorkTimesCitationCount(work_id=work1, count=15, nyt_corpus_frequency=0).save()
    NewYorkTimesCitationCount(work_id=work2, count=3, nyt_corpus_frequency=0).save()
    HathiTrustCitationCount(work_id=work1, count=10).save()
    HathiTrustCitationCount(work_id=work2, count=15).save()
    HathiTrustCitationCount(work_id=work3, count=16).save()

    WorkCataloguedByUser(user_id=1, work_id=1, status='want_to_read').save()
    WorkCataloguedByUser(user_id=1, work_id=2, status='read').save()

    DataSource(name="nyt", access_level=1, source_type="citation_count").save()
    DataSource(name="jstor", access_level=1, source_type="citation_count").save()
    DataSource(name="jstor_language_and_literature", access_level=1, source_type="citation_count").save()
    DataSource(name="google_scholar", access_level=1, source_type="citation_count").save()
    DataSource(name="alh", access_level=1, source_type="citation_count").save()
    DataSource(name="american_literature", access_level=1, source_type="citation_count").save()
    DataSource(name="nba", access_level=1, source_type="award_status").save()
    DataSource(name="pulitzer", access_level=1, source_type="award_status").save()
    DataSource(name="hathi_trust", access_level=1, source_type="citation_count").save()


def standard_custom_request(factory: RequestFactory):
    request = factory.get(
        "https://metacanon.org/09/custom/?total_books=500&books_per_page=100&ordered_by=score&min_year=1900&"
        "max_year=1999&weights_0=0.0&weights_1=0.0&weights_2=1.0&weights_3=0.5&weights_4=1.0&weights_5=1.0&"
        "weights_6=0.0&weights_7=0.0&genres=1&genres=2&genres=3&genres=4&faulkner=on")
    return request
