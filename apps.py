from django.apps import AppConfig


class MetacanonCoreConfig(AppConfig):
    name = 'metacanon_core'
